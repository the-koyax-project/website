# Code Contribution Guidelines

Thank you for your interest in contributing! If you would like to contribute your own code, feel free to [fork the project](https://gitlab.com/the-koyax-project/website/-/forks/new) and submit a [new merge request](https://gitlab.com/the-koyax-project/website/-/merge_requests).

# Bug reports and feature requests

Please file a [GitLab issue](https://gitlab.com/the-koyax-project/website/-/issues) if anything isn't working the way you expect or if you have a suggestion on how to improve the website.

# Contributors

**Owner**

- [Lars Buddenberg](https://gitlab.com/koyax)

**Maintainer**

- [Samuel Mock](https://gitlab.com/samumatic)

**Committers**

- [lluni](https://gitlab.com/lluni)