# Koyax Project Changelog

We are the Koyax Project, a non-profit association. We provide servers with open source software to protect your privacy.

This changelog summarizes updates to our mainpage.

## [Title] - [Date]

## Going Nord - 15 February, 2022
* From Darkly theme to a modified nord version
* Added PeerTube as 6th Service (and changed colors)
    + Added PeerTube 'more' page
* Moved donate banner to navbar
    + Added own donate page
* Added first beta picture to Koyax ID page

## Great Reverse - 30 January, 2022
* Deletion of en/ website
* New blog page with ghost
* Change from ’The koyax Project’ to ’Koyax Project’
* Deletion of Firefly instance and mentions on the website
* Changed from German formal language to personal language
* Changelog move to GitLab
* Updated text and links


## Languages - 03 Dezember, 2020
* The website is now in German and English available
* Changelog on the website

## Central Management System - 15 November, 2020
* Introduction of our central backend component management system
* Smaller bugfixes

## Various Fixes - 14 November, 2020
* Improved links
* Index 'Donate Button' deleted
* Various fixes under the hood

## After Fixes - 08 November, 2020
* Blog overview improvements
* Nextcloud description changes
* Index changes

## Version 2 - 07 November, 2020
* For the instances 'More' sites created
* 'About us' site created
* Blog overview created

## Initial Release - 06 November, 2020
* Index was published
* Fixed links and a 'Soon' site
