FROM nginx:latest

COPY koyax.org /var/www/website/koyax.org
COPY www.koyax.org /var/www/website/www.koyax.org
COPY peru.larsbuddenberg.de /var/www/website/peru.larsbuddenberg.de

COPY default.conf /etc/nginx/conf.d/default.conf

RUN chown -R www-data:www-data /var/www

EXPOSE 80
